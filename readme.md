# react-auto-subcomponent

create react components on-the-fly by simply accessing them!

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com) [![GitLabl Build Status](https://gitlab.com/cdaringe/react-auto-subcomponent/badges/master/build.svg)](https://gitlab.com/cdaringe/react-auto-subcomponent/commits/master)


## usage

the api is generally as follows:

```js
import { auto } from 'react-auto-subcomponent'
auto(
  ComponentClassToAttachDynamicComponentsTo,
  DynamicComponentClass,
  { // opts, defaults shown
    alphaMode: false // only permit dynamic components name A thru Z
  }
)
```
```jsx
// Thing.jsx
import { auto } from 'react-auto-subcomponent'
class Thing extends React.PureComponent {
  render () {
    return <div className='thing'>{...this.props.children}</div>
  }
}
export default auto(
  Thing,
  props => (
    <div className='thing-auto'>
      {props.children}
    </div>
  )
)

// now use it!
// App.js
import Thing from './Thing'
export default function App () {
  return (
    <Thing>
      <Thing.X>I never specified a `.X` component!</Thing.X>
      <Thing.Y>But it component renders using the 2nd arg to auto!</Thing.Y>
      <Thing.Z>This is useful for creating layout variants using</Thing.Z>
      <Thing.Q>similarly named components</Thing.Q>
    </Thing>
  )
}
```
