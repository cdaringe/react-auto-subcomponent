export function auto (Component, Child, opts) {
  var cache = {}
  opts = opts || {}
  if (!('alphaMode' in opts)) opts.alphaMode = false
  return new Proxy(Component, {
    get: function handleGetAutoComponent (obj, prop) {
      if (prop in obj) {
        return obj[prop]
      }
      if (cache[prop]) {
        return cache[prop]
      }
      var charCode = prop.charCodeAt()
      if (
        !opts.alphaMode ||
        (opts.alphaMode && charCode >= 65 && charCode <= 90)
      ) {
        var ChildComponent = Child
        ChildComponent.displayName = `${Component.displayName}.${
          ChildComponent.displayName
        }`
        cache[prop] = ChildComponent
        return ChildComponent
      }
    }
  })
}
