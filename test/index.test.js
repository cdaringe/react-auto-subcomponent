import React from 'react'
import Thing from './fixtures/Thing.react'
import renderer from 'react-test-renderer'
import { auto } from '../src/index'

test('PureComponent Thing creates children automatically', () => {
  const EXPECTED_TEXT = 'EXPECTED_TEXT'
  const componentX = renderer.create(<Thing.X>{EXPECTED_TEXT}</Thing.X>)
  let tree = componentX.toJSON()
  expect(tree).toMatchSnapshot()
})

test('Functional Stateless Component creates components automatically', () => {
  const FSC = auto(
    function FSC () {
      return <div />
    }, // empty component
    function FSCChild () {
      return <span>FSC.CHILD</span>
    }
  )
  const componentX = renderer.create(<FSC.B />)
  let tree = componentX.toJSON()
  expect(tree).toMatchSnapshot()
})

test('Handles many components', () => {
  // ${this.constructor.displayName}`}
  const Thing = auto(
    function Thingy (props) {
      return <div className='thing'>{props.children}</div>
    },
    function ThingSibling (props) {
      return <div className={`thing-auto`}>{props.children}</div>
    }
  )
  const componentX = renderer.create(
    <Thing>
      <Thing.X>Whoa, i never specified this!</Thing.X>
      <Thing.Y>But sure enough, this component renders!</Thing.Y>
      <Thing.Z>This is useful for creating layout variants using</Thing.Z>
      <Thing.Q>similarly named components</Thing.Q>
    </Thing>
  )
  let tree = componentX.toJSON()
  expect(tree).toMatchSnapshot()
})
