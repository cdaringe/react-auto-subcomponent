import React from 'react'
import { auto } from '../../src/index'

class Thing extends React.PureComponent {
  render () {
    return this.props.children
  }
}

function ThingAutoChild (props) {
  return props.children
}

export default auto(Thing, ThingAutoChild)
